package epam.test.task.stringsimilarity.Controller;

import epam.test.task.stringsimilarity.Service.SimilarityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SimilarityController {

    @Autowired
    @Qualifier(value = "cosineStringSimilarityServiceImpl")
    private SimilarityService similarityService;

    @GetMapping("/similarity/cosine/string/{s1}&{s2}")
    public Map<String, Integer> similarity_cosine_string(@PathVariable String s1, @PathVariable String s2){
        Map<String, Integer> map = new HashMap<>();
        Integer percentage = similarityService.getValue(s1,s2);
        map.put("percent",percentage);
        return map;
    }
}

package epam.test.task.stringsimilarity.Service.Impl;

import epam.test.task.stringsimilarity.Service.SimilarityService;
import epam.test.task.stringsimilarity.Service.StemmingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Service
public class CosineStringSimilarityServiceImpl implements SimilarityService {

    @Autowired
    private StemmingService stemmer;

    @Override
    public Integer getValue(String s1, String s2) {
        Integer similarityPercentage;

        if (!s1.equals("")&!s2.equals("")) {
            double similarityValue = cosineSimilarity(s1, s2);
            System.out.println(similarityValue);
            similarityPercentage = (int) Math.round(similarityValue * 100);
            System.out.println(similarityPercentage);
        } else {similarityPercentage = 0;}

        return similarityPercentage;
    }

    private Map<String, Integer> getTermFrequencyMap(String[] terms) {
        Map<String, Integer> termFrequencyMap = new HashMap<>();
        for (String term : terms) {
            Integer n = termFrequencyMap.get(term);
            n = (n == null) ? 1 : ++n;
            termFrequencyMap.put(term, n);
        }
        return termFrequencyMap;
    }

    private double cosineSimilarity(String text1, String text2) {
        //Get vectors
        Map<String, Integer> a = getTermFrequencyMap(stemmer.stemAllWordsInString(text1).toArray(new String[0])); //text1.split("\\W+")
        Map<String, Integer> b = getTermFrequencyMap(stemmer.stemAllWordsInString(text2).toArray(new String[0])); //text2.split("\\W+")

        //Get unique words from both sequences
        HashSet<String> intersection = new HashSet<>(a.keySet());
        intersection.retainAll(b.keySet());

        double dotProduct = 0, magnitudeA = 0, magnitudeB = 0;

        //Calculate dot product
        for (String item : intersection) {
            dotProduct += a.get(item) * b.get(item);
        }

        //Calculate magnitude a
        for (String k : a.keySet()) {
            magnitudeA += Math.pow(a.get(k), 2);
        }

        //Calculate magnitude b
        for (String k : b.keySet()) {
            magnitudeB += Math.pow(b.get(k), 2);
        }

        //return cosine similarity
        return dotProduct / Math.sqrt(magnitudeA * magnitudeB);
    }

}

package epam.test.task.stringsimilarity.Service.Impl;

import epam.test.task.stringsimilarity.Service.StemmingService;
import opennlp.tools.stemmer.PorterStemmer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PorterStemmingServiceImpl implements StemmingService{

    @Override
    public List<String> stemAllWordsInString(String s) {
        List<String> words = Arrays.asList(s.split("\\W+"));
        List<String> stemmedWords = new ArrayList<String>();

        PorterStemmer porterStemmer = new PorterStemmer();
        words.forEach(word -> {
            String stemmed = porterStemmer.stem(word);
            stemmedWords.add(stemmed);
            System.out.println(word+": "+stemmed);
        });
        return stemmedWords;
    }
}

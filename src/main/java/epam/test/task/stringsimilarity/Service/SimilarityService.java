package epam.test.task.stringsimilarity.Service;

public interface SimilarityService {

    Integer getValue(String s1, String s2);

}

package epam.test.task.stringsimilarity.Service;

import java.util.List;

public interface StemmingService {

    List<String> stemAllWordsInString(String s);

}

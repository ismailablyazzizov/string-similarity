package epam.test.task.stringsimilarity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringSimilarityApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringSimilarityApplication.class, args);
	}

}

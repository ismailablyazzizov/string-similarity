$( document ).ready(function() {

    $(".cosine_string_input").keyup(function(e) {
        delay(function(){
          //console.log(e.target.id + " has changed;");
          getCosineStringSimilarity($("#cosine_string_one").val(), $("#cosine_string_two").val());
        }, 1000 );

    });

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    function getCosineStringSimilarity(s1, s2) {
       //console.log(s1+s2);

       var result;
       $.ajax({
          type:'GET',
          url:"/similarity/cosine/string/"+s1+"&"+s2,
          headers: {
              Accept: 'application/json'
          },
          dataType: 'json',
          success:function(data){
              //console.log(data);
              result = data.percent;
              console.log("qq: "+result);
              updateProgress(".progress_cosine_string",result);
          },
          error: function() {
              result = 77;
              console.log("getCosineStringSimilarity  FAILED");
          }
       });

    }

    function updateProgress(progressClass, percent){
      var delay = 500;
      $(progressClass).each(function(i){
          $(this).delay( delay*i ).animate( { width: percent + '%' }, delay );
          $(this).prop('Counter',0).animate({
              Counter: percent
          }, {
              duration: delay,
              easing: 'swing',
              step: function (percent) {
                  $(this).text(Math.ceil(percent)+'%');
              }
          });
      });
    }
});
